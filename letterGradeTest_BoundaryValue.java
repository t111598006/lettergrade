import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class letterGradeTest_BoundaryValue {
    @Test
    public void testGradeA() {
        assertEquals('A', letterGrade.letterGrade(100));
        assertEquals('A', letterGrade.letterGrade(90));
    }
    @Test
    public void testGradeB() {
        assertEquals('B', letterGrade.letterGrade(89));
        assertEquals('B', letterGrade.letterGrade(80));
    }
    @Test
    public void testGradeC() {
        assertEquals('C', letterGrade.letterGrade(79));
        assertEquals('C', letterGrade.letterGrade(70));
    }
    @Test
    public void testGradeD() {
        assertEquals('D', letterGrade.letterGrade(69));
        assertEquals('D', letterGrade.letterGrade(60));
    }
    @Test
    public void testGradeF() {
        assertEquals('F', letterGrade.letterGrade(59));
        assertEquals('F', letterGrade.letterGrade(0));
    }
    @Test
    public void testInvalidScore() {
        assertEquals('X', letterGrade.letterGrade(-1));
        assertEquals('X', letterGrade.letterGrade(101));
    }
}
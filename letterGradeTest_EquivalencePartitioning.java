import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class letterGradeTest_EquivalencePartitioning {
    @Test
    public void testGradeA() {
        assertEquals('A', letterGrade.letterGrade(95));
    }
    @Test
    public void testGradeB() {
        assertEquals('B', letterGrade.letterGrade(85));
    }
    @Test
    public void testGradeC() {
        assertEquals('C', letterGrade.letterGrade(75));
    }
    @Test
    public void testGradeD() {
        assertEquals('D', letterGrade.letterGrade(65));
    }
    @Test
    public void testGradeF() {
        assertEquals('F', letterGrade.letterGrade(30));
    }
    @Test
    public void testInvalidScore() {
        assertEquals('X', letterGrade.letterGrade(-100));
        assertEquals('X', letterGrade.letterGrade(200));
    }
}
